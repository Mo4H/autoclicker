﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AutoClicker.Mouse {

    public class MouseHookEventArgs : EventArgs {

        protected MouseButtons _button;
        protected MouseState _mouseState;
        protected Point _location;

        public MouseHookEventArgs(MouseButtons button, MouseState mouseState, int x, int y){
            _button = button;
            _mouseState = mouseState;
            _location = new Point(x, y);
        }

        public MouseButtons Button { get { return _button; }  }
        public MouseState MouseState { get { return _mouseState; } }
        public Point Location { get { return _location; } }
        public int X { get { return _location.X; } }
        public int Y { get { return _location.Y; } }
        public override String ToString() { return _button + " mouse button " + " state: " + _mouseState + " at " + _location.ToString(); }
    }
}
