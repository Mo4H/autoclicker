﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace System.Windows.Forms {

    public class MouseEvent : EventArgs {

        private MouseButtons _button;
        private bool _up;
        private Point _location;

        public MouseEvent(MouseButtons button, bool up, int x, int y){
            _button = button;
            _up = up;
            _location = new Point(x, y);
        }

        public MouseButtons Button { get { return _button; }  }
        public bool Up { get {return _up;} }
        public bool Down { get { return !_up; } }
        public Point Location { get { return _location; } }
        public int X { get { return _location.X; } }
        public int Y { get { return _location.Y; } }
    }
}
