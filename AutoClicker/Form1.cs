﻿using AutoClicker.Mouse;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Windows.Input;

namespace AutoClicker {
    public partial class Form1 : Form {

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        private const int MOUSEEVENTF_WHEEL = 0x800;

        static System.Timers.Timer _timerClick;
        static System.Timers.Timer _timerWait;
        static System.Timers.Timer _timerEat;

        static bool _eating = false;
        static int amountOfEating = 4;
        static int countEating = 0;
        static bool rightPressed = false;
        static bool wait = true;
        private bool _mouseDown = false;

        private static Point prevPos = new Point();

        public Form1() {
            InitializeComponent();
            initTimers();


            MouseHook.Start();
            MouseHook.MouseEvent += OnMouseEvent;
            MouseHook.MouseMoveEvent += OnMouseMovedEvent;
            MouseHook.MouseScrollEvent += OnMouseScollEvent;

            KeyBoardHook.Start();
            KeyBoardHook.KeyEvent += OnKeyEvent;
        }

        public static void initTimers() {
            _timerClick = new System.Timers.Timer(100);
            _timerClick.Elapsed += new ElapsedEventHandler(OnTimer);
            _timerClick.Enabled = true; // Enable it
            _timerWait = new System.Timers.Timer(2000);
            _timerWait.Elapsed += new ElapsedEventHandler(OnTimerWait);
            _timerWait.Enabled = true;
            _timerEat = new System.Timers.Timer(6000);
            _timerEat.Elapsed += new ElapsedEventHandler(OnTimerEat);
            _timerEat.Enabled = true;

        }

        private void OnMouseEvent(object sender, MouseHookEventArgs e) {
            if (e.Button != MouseButtons.Left) {
                ResetWait();
                Debug.WriteLine(e.ToString());
            } else {
                ResetWait();
            }
        }

        private void OnMouseScollEvent(object sender, MouseHookEventArgs e) {
            ResetWait();
        }

        private void OnMouseMovedEvent(object sender, MouseHookEventArgs e) {
            ResetWait();
        }

        private void OnKeyEvent(object sender, KeyEventArgs e) {
            ResetWait();
        }

        public static void OnTimer(object sender, ElapsedEventArgs e) {
            if (_eating == true) return;
            Point mousPos = Cursor.Position;
            if (prevPos.Equals(mousPos) && wait == false) {
                MouseClick();
                wait = false;
                prevPos = mousPos;
                _timerWait.Enabled = wait;
                _timerClick.Enabled = !wait;
            } else {
                wait = true;
                prevPos = mousPos;
                _timerWait.Enabled = wait;
                _timerClick.Enabled = !wait;
            }
        }

        public static void OnTimerWait(object sender, ElapsedEventArgs e) {
            if (!_eating) {
                Point mousPos = Cursor.Position;
                if (prevPos.Equals(mousPos) && wait == true) {
                    wait = false;
                    prevPos = mousPos;
                    _timerWait.Enabled = wait;
                    _timerClick.Enabled = !wait;
                } else {
                    wait = true;
                    prevPos = mousPos;
                    _timerWait.Enabled = wait;
                    _timerClick.Enabled = !wait;
                }
            } else {
                if (countEating < amountOfEating){
                    MouseRightPress();
                }else{
                    countEating = 0;
                    MouseRightRelease();
                    _eating = false;
                    MouseScroll(4, true);
                }
                
            }
        }

        public static void OnTimerEat(object sender, ElapsedEventArgs e) {
            _eating = true;
            MouseScroll(4, false);
        }

        public static void MouseClick() {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            mouse_event(MOUSEEVENTF_LEFTDOWN, x, y, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, x, y, 0, 0);
        }

        public static void MouseScroll(int amountScroll, bool up) {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            for (int i = 0; i < amountScroll; i++) {
                if (up) {
                    mouse_event(MOUSEEVENTF_WHEEL, x, y, 1, 0);
                } else {
                    mouse_event(MOUSEEVENTF_WHEEL, x, y, -1, 0);
                }
            }
        }

        public static void MouseRightPress() {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            mouse_event(MOUSEEVENTF_RIGHTDOWN, x, y, 0, 0);
            rightPressed = true;
        }

        public static void MouseRightRelease() {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            mouse_event(MOUSEEVENTF_RIGHTUP, x, y, 0, 0);
            rightPressed = false;
        }

        private static void ResetWait() {
            wait = true;
            TimerHelper.Reset(_timerWait);
            TimerHelper.Reset(_timerEat);
            _eating = false;
            if(rightPressed == true){
                MouseRightRelease();
            }

        }
    }
}
