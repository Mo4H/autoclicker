﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoClicker.Mouse {
    public enum MouseState {
        UP,
        DOWN,
        CLICK,
        MOVE,
        SCROLL
    }
}
