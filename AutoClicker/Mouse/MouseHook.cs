﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoClicker.Mouse {
    public static class MouseHook {

        public static event MouseHookEventHandler MouseEvent = delegate { };
        public static event MouseHookEventHandler MouseMoveEvent = delegate { };
        public static event MouseHookEventHandler MouseScrollEvent = delegate { };

        public static void Start() {
            _hookID = SetHook(_proc);
        }

        public static void stop() {
            UnhookWindowsHookEx(_hookID);
        }

        private static LowLevelMouseProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        private static IntPtr SetHook(LowLevelMouseProc proc) {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule) {
                return SetWindowsHookEx(WH_MOUSE_LL, proc,
                  GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelMouseProc( int nCode, IntPtr wParam, IntPtr lParam );

        private static IntPtr HookCallback( int nCode, IntPtr wParam, IntPtr lParam) {
            if( nCode >= 0){
                MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                MouseMessages currentMouseMsg = (MouseMessages)wParam;
                if (MouseMessages.WM_MOUSEMOVE == currentMouseMsg) {
                    MouseMoveEvent(null, new MouseHookEventArgs(MouseButtons.None, MouseState.MOVE, hookStruct.pt.x, hookStruct.pt.y));
                } else if (MouseMessages.WM_MOUSEWHEEL != currentMouseMsg) {
                    MouseEvent(null, new MouseHookEventArgs(GetMouseButton(currentMouseMsg), GetMouseButtonState(currentMouseMsg), hookStruct.pt.x, hookStruct.pt.y));
                } else if(MouseMessages.WM_MOUSEWHEEL == currentMouseMsg) {
                    MouseScrollEvent(null, new MouseHookScrollEventArgs(GetMouseButton(currentMouseMsg), GetMouseButtonState(currentMouseMsg), hookStruct.pt.x, hookStruct.pt.y, 1));
                }
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        private const int WH_MOUSE_LL = 14;

        private enum MouseMessages {
            WM_MOUSEMOVE = 0x200,
            WM_LBUTTONDOWN = 0x201,
            WM_LBUTTONUP = 0x202,
            WM_LBUTTONDBLCLK = 0x203,
            WM_RBUTTONDOWN = 0x204,
            WM_RBUTTONUP = 0x205,
            WM_RBUTTONDBLCLK = 0x206,
            WM_MBUTTONDOWN = 0x207,
            WM_MBUTTONUP = 0x208,
            WM_MBUTTONDBLCLK = 0x209,
            WM_MOUSEWHEEL = 0x20A,
            WM_XBUTTONDOWN = 0x20B,
            WM_XBUTTONUP = 0x20C,
            WM_XBUTTONDBLCLK = 0x20D,
            WM_MOUSEHWHEEL = 0x20E
        }

        private static MouseButtons GetMouseButton(MouseMessages mouseMessage) {
            if (mouseMessage == MouseMessages.WM_LBUTTONDOWN || mouseMessage == MouseMessages.WM_LBUTTONUP || mouseMessage == MouseMessages.WM_LBUTTONDBLCLK) {
                return MouseButtons.Left;
            } else if (mouseMessage == MouseMessages.WM_MBUTTONDOWN || mouseMessage == MouseMessages.WM_MBUTTONUP || mouseMessage == MouseMessages.WM_MBUTTONDBLCLK) {
                return MouseButtons.Middle;
            } else if (mouseMessage == MouseMessages.WM_RBUTTONDOWN || mouseMessage == MouseMessages.WM_RBUTTONUP || mouseMessage == MouseMessages.WM_RBUTTONDBLCLK) {
                return MouseButtons.Right;
            } else if (mouseMessage == MouseMessages.WM_XBUTTONDOWN || mouseMessage == MouseMessages.WM_XBUTTONUP || mouseMessage == MouseMessages.WM_XBUTTONDBLCLK) {
                return MouseButtons.XButton1;
            } else {
                return MouseButtons.None;
            }
        }
        private static MouseState GetMouseButtonState(MouseMessages mouseMessage) {
            if (mouseMessage == MouseMessages.WM_LBUTTONDOWN || mouseMessage == MouseMessages.WM_MBUTTONDOWN || mouseMessage == MouseMessages.WM_RBUTTONDOWN) {
                return MouseState.DOWN;
            } else if (mouseMessage == MouseMessages.WM_LBUTTONUP || mouseMessage == MouseMessages.WM_MBUTTONUP || mouseMessage == MouseMessages.WM_RBUTTONUP) {
                return MouseState.UP;
            } else if (mouseMessage == MouseMessages.WM_LBUTTONDBLCLK || mouseMessage == MouseMessages.WM_MBUTTONDBLCLK || mouseMessage == MouseMessages.WM_RBUTTONDBLCLK) {
                return MouseState.CLICK;
            } else if (mouseMessage == MouseMessages.WM_MOUSEMOVE) {
                return MouseState.MOVE;
            } else {
                return MouseState.SCROLL;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct POINT {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct MSLLHOOKSTRUCT {
            public POINT pt;
            public uint mouseData;
            public uint flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
          LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
          IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);


    }
}
