﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoClicker.Mouse {
    class MouseHookScrollEventArgs : MouseHookEventArgs {

        protected int _delta;

        public MouseHookScrollEventArgs(MouseButtons button, MouseState mouseState, int x, int y, int delta):base(button, mouseState, x, y) {
            _delta = delta;
        }
        public int Delta { get { return _delta; } }
    }
}
